gulp = require('parent-require')('gulp')
tap = require 'gulp-tap'
async = require 'async'

extractor = require './lib/extractor'

module.exports = (options, sources)->
  gulp.task "translate", (done)->
    async.forEachOfSeries sources, (src, key, cb)->
      files = []
      gulp.src src
      .pipe tap (file)->
        files.push(file)
      .on 'end', ()->
        extractor.extract(options.project, key, files)
        .finally ()->
          cb()
    , ()->
      extractor.reportDuplicates(options.project)
      .finally ()->
        done()
    return