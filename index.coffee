translator = require './lib/translator'
translatorProps = require './lib/translatorProps'
translatorTypes = require './lib/translatorTypes'
extractor = require './lib/extractor'

class ZsTranslations
  getTranslator: ()->
    return translator

  getTranslatorProps: ()->
    return translatorProps

  getTranslatorTypes: ()->
    return translatorTypes
  
  getExtractor: ()->
    return extractor  
      
module.exports = new ZsTranslations()