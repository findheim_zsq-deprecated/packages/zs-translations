chalk = require 'chalk'
mkdirp = require 'mkdirp'
_ = require 'lodash'

mongo = require './mongo'
utils = require './utils'

class Translator
  constructor: ()->
    @translations = {}
    @missingTranslations = {}
    
  loadFromDb: (project, country)->
    mongo.getTranslations(project, country, true)
    .then (translations)=>
      @translations = translations
    .catch (err)->
      utils.printError "Translator", "Load from db failed", err.message
      throw err

  build: (project, country)->
    translator.loadFromDb project, country
    .then (translations)=>
      mkdirp.sync '.build/translations'
      fs.writeFileSync '.build/translations/project.json', JSON.stringify(translations)
      return
    
  loadFromFile: ()->
    try
      @translations = JSON.parse(fs.readFileSync('.build/translations/project.json','utf8'))
    catch err
      utils.printError "Translator", "Load from file failed", err.message
      throw new Error("could not load translations from file (#{err.message})")
    
  translate: (key, language, params)->
    if @translations[key]?.translations?[language]?.text?
      result = @translations[key].translations[language].text
      for param, index in utils.extractParams(params)
        result = result.replace(new RegExp("\\$#{index}","gi"), param)
      return result
    else
      @missingTranslations[language] = @missingTranslations[language] or []
      @missingTranslations[language].push key
      return ''

  translateText: (text, language)->
    text.replace utils.regexHtml, (match, a, key, params)=>
      @translate key, language, params
        
  report: ()->
    if _.size(@missingTranslations) > 0
      utils.printError "Translator", "Translations missing"
      for country, keys of @missingTranslations
        for key in keys
          console.log chalk.magenta(country) + " - " + key
    
module.exports = new Translator()