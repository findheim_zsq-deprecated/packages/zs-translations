mkdirp = require 'mkdirp'
_ = require 'lodash'
q = require 'q'
request = require 'request'
utils = require './utils'
fs = require 'fs'

class Translator
  constructor: ()->
    @translations = {}

  loadFromServer: ()->
    q.all [
      @_loadLanguage('de')
      @_loadLanguage('en')
    ]
    .then (translations)=>
      @translations['de'] = translations[0]
      @translations['en'] = translations[1]
      return @translations

  build: ()->
    @loadFromServer()
    .then (translations)=>
      mkdirp.sync '.build/translations'
      fs.writeFileSync '.build/translations/props.json', JSON.stringify(translations)
      return

  loadFromFile: ()->
    try
      @translations = JSON.parse(fs.readFileSync('.build/translations/props.json','utf8'))
    catch err
      utils.printError "Translator", "Load from file failed", err.message
      throw new Error("could not load translations from file (#{err.message})")

  translate: (key, language)->
    if @translations[language]?[key]?
      return @translations[language][key]
    else
      return ''

  getTranslations: (language)->
    if @translations[language]?
      return @translations[language]
    else
      return {}

  _loadLanguage: (language)->
    def = q.defer()
    request
      uri: "https://metadata.zoomsquare.com/xtr/metadata/getProperties?lang=" + language
      method: "GET"
      json: true
    , (err, msg, body)=>
      if err?
        def.reject(err)
      else
        lang = {}
        for key, obj of body
          if obj.label?
            lang[key] = obj.label
        def.resolve lang
    def.promise

module.exports = new Translator()