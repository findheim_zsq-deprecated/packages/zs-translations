async = require 'async'
_ = require 'lodash'
chalk = require 'chalk'
path = require 'path'
q = require 'q'

mongo = require './mongo'
utils = require './utils'

class Extractor
  extractFromFiles: (project, country, files)->
    console.log chalk.blue("Extractor") + ": " + "Extraction start (#{project} - #{country})"
    translations = {}
    for file in files
      location = file.path.substring(process.cwd().length + 1).replace(/\\/g,'/') #normalized relative path
      # console.log location
      for item in @_extractFile file
        lines = file.contents.toString().substring(0, item.index).split('\n')
        if translations[item.key]?
          translations[item.key].locations.push
            project: project
            country: country
            file: location
            line: lines.length
            column: lines[lines.length-1].length
            params: item.params
        else
          translations[item.key] =
            key: item.key
            locations: [
              project: project
              country: country
              file: location
              line: lines.length
              column: lines[lines.length-1].length
              params: item.params
            ]
    console.log chalk.blue("Extractor") + ": " + "Extraction finished (#{project} - #{country})"
    return translations
    
  _extractFile: (file)->
    if path.extname(file.path) == '.html'
      return @_extractHtml(file.contents)

    if path.extname(file.path) == '.pug'
      return @_extractPug(file.contents)

    if path.extname(file.path) == '.ts'
      return @_extractTs(file.contents.toString('utf8'))

    return []

  _extractHtml: (contents)->
    res = []
    while item = utils.regexHtml.exec(contents)
      res.push
        index: item.index
        key: item[2]
        params: utils.extractParams(item[3])
    return res

  _extractPug: (contents)->
    res = []
    while item = utils.regexPug.exec(contents)
      res.push
        index: item.index
        key: item[1]
        params: utils.extractParams(item[3])
    return res

  _extractTs: (contents)->
    res = []
    while item = utils.regexTs.exec(contents)
      res.push
        index: item.index
        key: item[1]
        params: utils.extractParams(item[3])
    return res

  updateTranslations: (project, country, translations)->
    def = q.defer()
    q.all [
      mongo.getTranslations(project, country)
      mongo.getTranslations('common')
    ]
    .spread (projectTranslations, commonTranslations)=>
      console.log chalk.blue("Extractor") + ": " + "Merge & Update start (#{project} - #{country})"
      stats =
        counts:
          extracted: _.size translations
          loaded: _.filter(projectTranslations, (val)-> return val.status != "unused").length
          afterMerge: 0
          common:
            loaded: _.size commonTranslations
            usedAfter: 0
        project: {added: 0, changed: 0, removed: 0}
        common: {added: 0, changed: 0, removed: 0}
      async.forEachOfSeries translations, (item, key, done)=>
        if key.indexOf('common-') == 0
          stats.counts.common.usedAfter++
          if commonTranslations[key]
            newItem = @_updateCommonTranslation project, country, commonTranslations[key], item
            if newItem
              mongo.updateTranslation newItem
              .then ()->
                stats.common.changed++
                delete commonTranslations[key]
                done()
              .catch (err)->
                done err
            else
              delete commonTranslations[key]
              setTimeout done, 0
          else
            mongo.addTranslation "common", "shared", item
            .then ()->
              stats.common.added++
              done()
            .catch (err)->
              done err
        else
          stats.counts.afterMerge++
          if projectTranslations[key]
            newItem = @_updateTranslation projectTranslations[key], item
            if newItem?
              mongo.updateTranslation newItem
              .then ()->
                stats.project.changed++
                delete projectTranslations[key]
                done()
              .catch (err)->
                done err
            else
              delete projectTranslations[key]
              setTimeout done, 0
          else
            mongo.addTranslation project, country, item
            .then ()->
              stats.project.added++
              done()
            .catch (err)->
              done err
      , (err)->
        if err?
          utils.printError "Extractor", "Error in Merge & Update", err.message
          def.reject err
        else
          console.log chalk.blue("Extractor") + ": " + "Merge & Update finished (#{project} - #{country})"
          console.log chalk.blue("Extractor") + ": " + "Cleanup project start (#{project} - #{country})"
          async.forEachOfSeries projectTranslations, (item, key, done)->
            if item.status != "unused"
              item.status = "unused"
              mongo.updateTranslation item
              .then ()->
                stats.project.removed++
                done()
              .catch (err)->
                done err
            else #unchanged
              setTimeout done, 0
          , (err)->
            if err?
              utils.printError "Extractor", "Error in Cleanup project", err.message
              def.reject err
            else
              console.log chalk.blue("Extractor") + ": " + "Cleanup project finished (#{project} - #{country})"
              console.log chalk.blue("Extractor") + ": " + "Cleanup common start (#{project} - #{country})"
              async.forEachOfSeries commonTranslations, (item, key, done)->
                removedElements = _.remove(item.locations,{project: project, country: country})
                if removedElements.length > 0
                  mongo.updateTranslation item
                  .then ()->
                    stats.common.removed++
                    done()
                  .catch (err)->
                    done err
                else
                  setTimeout done, 0
              , (err)->
                if err?
                  utils.printError "Extractor", "Error in Cleanup common", err.message
                  def.reject err
                else
                  console.log chalk.blue("Extractor") + ": " + "Cleanup common finished (#{project} - #{country})"
                  console.log chalk.green("Extract complete") + ": " + JSON.stringify(stats, null, 2)
                  def.resolve()
    .catch (err)->
      utils.printError "Extractor", "Error in loading translations", err.message
      def.reject err
    def.promise

  _updateTranslation: (oldTranslation, newTranslation)->
    changed = false
    if oldTranslation.status == "unused"
      changed = true
      oldTranslation.status = "dirty"
      for key, elem of oldTranslation.translations
        elem.status = "dirty"
    unless _.isEqual oldTranslation.locations, newTranslation.locations
      changed = true
      oldTranslation.locations = newTranslation.locations
    if changed
      return oldTranslation
    return null

  _updateCommonTranslation: (project, country, oldTranslation, newTranslation)->
    changed = false
    newLocations = []
    for loc in oldTranslation.locations
      if loc.project == project and loc.country == country
        index = @_findLocation(newTranslation.locations, loc)
        if index >= 0
          newTranslation.locations.splice index, 1
          newLocations.push loc
        else
          changed = true
      else
        newLocations.push loc
    if newTranslation.locations.length > 0
      changed = true
      newLocations = newLocations.concat newTranslation.locations
    if changed
      oldTranslation.locations = newLocations
      return oldTranslation
    return null
    
  _findLocation: (locations, loc)->
    for item, index in locations
      if _.isEqual item, loc
        return index
    return -1

extractor = new Extractor() 
module.exports =
  extract: (project, country, files)->
    translations = extractor.extractFromFiles project, country, files
    extractor.updateTranslations project, country, translations
  reportDuplicates: (project)->
    mongo.getTranslations project
    .then (translations)->
      shared = {}
      country = []
      duplicates = []
      for key, translation of translations
        if translation.country == 'shared'
          shared[key] = true
        else
          country.push key
      for key in country
        if shared[key]?
          duplicates.push key
      if duplicates.length > 0
        utils.printError "Extractor", "Duplicates", "Found #{duplicates.length} non unique keys"
        console.log duplicates.join("\n")
      return
    .catch (err)->
      utils.printError "Extractor", "Duplicates", "Could not load translations"
      throw err
          