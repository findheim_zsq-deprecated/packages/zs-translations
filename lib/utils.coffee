chalk = require 'chalk'

class Utils
  regexHtml: /\[\[\s*translate\s*(['"])(.*?)\1\s*([\s\S]*?)\s*\]\]/gi
  regexParam: /(['"´`])([\s\S]*?)\1/gi

  regexPug: /\_\_\(['"]([\w-]+?)['"][,\s\w]*\)/gi

  regexTs: /Translations\.translate\('([\w-]+?)'/gi

  extractParams: (parameters)->
    result = []
    if parameters?.length > 0
      while param = @regexParam.exec(parameters)
        result.push param[2]
    return result

  printError: (module, title, message)->
    console.error chalk.red("ERROR") + " - " + chalk.yellow("Translation - #{module}\n")
    if message?
      console.error chalk.magenta(title) + ": " + message
    else
      console.error chalk.magenta(title)
  
module.exports = new Utils()