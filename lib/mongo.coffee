q = require("q")
schemas = require("parent-require")("zs-schemas")

Translations = schemas.model "TranslationsV2", "live"

class Mongo
  getTranslations: (project, country, includeMutual)->
    unless project?
      return q.reject new Error("param project missing")
    def = q.defer()
    if includeMutual?
      query =
        $or: [
          {project: project}
          {project: {$in: ["web-basic","common"]}}
        ]
      if country?
        query["$or"][0].country = {$in: [country, "shared"]}
    else
      query =
        project: project
      if country?
        query.country = country
    Translations.find(query).lean().exec (err, res)->
      if err?
        def.reject err
      else
        translations = {}
        for translation in res
          translations[translation.key] = translation
        def.resolve translations
    def.promise

  addTranslation: (project, country, translation)->
    unless project?
      return q.reject new Error("param project missing")
    unless project?
      return q.reject new Error("param translation missing")
    translation.project = project
    if country?
      translation.country = country
    translation.status = "dirty"
    item = new Translations translation
    def = q.defer()
    item.save (err, res)->
      if err?
        def.reject err
      else
        def.resolve res
    def.promise

  updateTranslation: (translation)->
    unless translation?._id?
      return q.reject new Error("id of param translation missing")
    def = q.defer()
    translation.changed = new Date()
    Translations.update({_id: translation._id}, translation).exec (err, res)->
      if err?
        def.reject err
      else
        def.resolve res
    def.promise

module.exports = new Mongo()